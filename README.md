Todo application
===================

Aplikacja podzielona jest na 2 foldery:

 - Frontend
 - Backend

Frontend
--------
Pliki frontendu umieszczono w katalogu `/frontend`
Frontend aplikacji napisany został w przy użyciu VanilaJS, HTML oraz preprocesora SASS. 

Pliki JavaScript podzielono na moduły i umieszczono w folderze `/frontend/src/js`. Do ich kompilacji oraz scalenia użyty został Webpack. 
Aby zbudować aplikację należy wykonać kroki:

    cd frontend
    npm install
    npm run build
Wygenerowany zostanie wynikowy plik `bundle.js` znajdujący się w folderze `/backend/static`. Do tej lokalizacji przekopiowany zostanie również szablon `index.html`.

Pliki `.scss` są kompilowane przy użyciu pakietu `node-sass`. Aby wygenerować arkusz `.css` należy dokonać operacji:

    npm install -g node-sass
    npm run sass
Wygenerowany zostanie wynikowy plik `app.css` znajdujący się w folderze `/backend/static`.

Wygenerowane pliki hostowane będą statycznie przez backend aplikacji.

Backend
=======

Backend aplikacji napisany został przy użyciu Node.js i znajduje się w folderze `/backend`.
W celu uruchomienia aplikacji należy wykonać kroki:

    cd backend
    npm install
    node app.js
Aplikacja uruchomi się na adesie lokalnym: `localhost:80`
Aby wygenerować na nowo bazę danych uruchomić należy skrypt:

    node initDatabase.js


> Bezpośrednio po forku repozytorium w folderze `backend/static` znajdują się wygenerowane pliki frontendu:
> `app.css
> bundle.js
> index.html`