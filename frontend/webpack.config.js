var path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    entry: {
        app: ["./src/js/app.js"]
    },
    output: {
        path: path.resolve(__dirname, "../backend/static"),
        publicPath: "/",
        filename: "bundle.js"
    },
    module: {
        rules: [
        ]
    },
    plugins: [new HtmlWebpackPlugin({
        template: 'index.html'
    })],
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:3000',
                secure: false
            }
        }
    },

};

//    Debug dependencies
//    "css-loader": "^0.28.3",
//    "style-loader": "^0.18.1",
//    "raw-loader": "^0.5.1",
//    "sass-loader": "^6.0.5",

//      Loaders used for debugging and developing
// {
//     test: /\.scss$/,
//     loaders: ['style-loader', 'css-loader', 'sass-loader']
// },
// {
//     test: /\.html$/,
//     loader: "raw-loader"
// }