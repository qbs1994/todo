/**
 * MVC aplications controller
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 *
 * @param {Object} model
 * @param {Object} view
 * @constructor
 */
export function Controller(model, view) {
    var _this = this;

    this._model = model;
    this._view = view;

    this._view.addButtonClicked.attach(function () {
        _this.addItem();
    });

    this._view.delButtonClicked.attach(function (sender, args) {
        _this.delItem(args.id);
    });

    this._view.doneCheckboxChanged.attach(function (sender, args) {
        _this.doItem(args.id);
    });

    this._view.inputTextChanged.attach(function (sender, args) {
        _this.updateInputText(args.text);
    })
}

Controller.prototype = {

    /**
     * Add new item
     */
    addItem : function () {
        var item = {
            title: this._model.getInputText(),
            done: false
        };
        if (item.title.length > 1) {
            this._model.addItem(item);
            this._model.clearInputText();
            this._view.setIconError(false);
        }
        else{
            this._view.setIconError(true);
        }
    },

    /**
     * Delete item
     * @param {number} id - Id of deleted item
     */
    delItem : function (id) {
        this._model.removeItem(id);
    },
    /**
     * Change "done" state of item
     * @param id - Id of updated item
     */
    doItem : function (id) {
        this._model.doItem(id);
    },

    /**
     * Update input state
     * @param {string} text - Input text
     */
    updateInputText : function (text) {
        this._model.setInputText(text);
        if (text.length > 1)
            this._view.setIconError(false);
    }
};
