/**
 * Implementation of observer pattern
 * @param {Object} sender - Object which emits events
 * @constructor
 */
export function Event(sender) {
    this._sender = sender;
    this._listeners = [];
}

Event.prototype = {

    /**
     * Attach new listener
     * @param {object} listener - callback function
     */
    attach : function (listener) {
        this._listeners.push(listener);
    },

    /**
     * Notify listeners about actions
     * @param args
     */
    notify : function (args) {
        var index;

        for (index = 0; index < this._listeners.length; index += 1) {
            this._listeners[index](this._sender, args);
        }
    }
};
