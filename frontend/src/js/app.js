//  Used for debugging and developing
// import '../../index.html'
// import '../sass/app.scss'


import { Model } from './model'
import { View } from './view'
import { Controller } from './controller'


document.addEventListener('DOMContentLoaded', function() {
    var model = new Model(),
        view = new View(model, {
            'list' : document.getElementById('list'),
            'inputText': document.getElementById('input-text'),
            'addButton' : document.getElementById('add-new-todo'),
            'iconError' : document.getElementById('icon-error'),
            'iconConnection' : document.getElementById('icon-connection'),
        }),
        controller = new Controller(model, view);
    model.fetchList();
    view.rebuildList();
});
