import {Event} from './event'

/**
 * MVC applications view
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 * @param {object} model
 * @param {Object} elements - Object that contains handlers to DOM control elements
 * @constructor
 */

export function View(model, elements) {
    var _this = this;

    this._model = model;
    this._elements = elements;

    this.addButtonClicked = new Event(this);
    this.delButtonClicked = new Event(this);
    this.doneCheckboxChanged = new Event(this);
    this.inputTextChanged = new Event(this);


    this._model.itemAdded.attach(function () {
        _this.rebuildList();
    });
    this._model.listChanged.attach(function () {
        _this.rebuildList();
    });
    this._model.inputTextClear.attach(function () {
        _this.inputTextClear();
    })
    this._model.connectionError.attach(function (sender, args) {
        _this.setIconConnection(args.status)
    })

    this._elements.addButton.onclick = function () {
        _this.addButtonClicked.notify();
    };

    this._elements.inputText.onkeyup = function (e) {
        _this.inputTextChanged.notify({text: e.target.value});
    }
}

View.prototype = {

    /**
     *  Function which rebuilds list of todos
     *  It looks terrible, but it's made without external libs.
     *  Template rendering package would make code more pure.
     */
    rebuildList: function () {
        var list, items, key;

        list = this._elements.list;
        list.innerHTML = '';

        items = this._model.getItems();
        for (key in items) {
            if (items.hasOwnProperty(key)) {
                let id = items[key].id;
                var _this = this;

                var divItem = document.createElement('div');
                divItem.setAttribute('class', 'item');
                var divBox = document.createElement('div');
                divBox.setAttribute('class', 'box');
                var inputCheckbox = document.createElement('input');
                inputCheckbox.setAttribute('type', 'checkbox');
                inputCheckbox.setAttribute('id', 'item-select-' + items[key].id);
                if (items[key].done){
                    inputCheckbox.setAttribute('checked', true);
                }
                inputCheckbox.onchange = function(){
                    _this.doneCheckboxChanged.notify({id: id});
                };
                var labelCheckbox = document.createElement('label');
                labelCheckbox.setAttribute('for', 'item-select-' + items[key].id);
                var span = document.createElement('span');
                span.setAttribute('class', 'checkbox');
                var h3 = document.createElement('h3');
                if (items[key].done){
                    h3.setAttribute('class', 'done')
                }
                var text = document.createTextNode(items[key].title);
                h3.appendChild(text);
                var divDelete = document.createElement('div');
                if (items[key].done) {
                    divDelete.setAttribute('class', 'delete done');
                }
                else{
                    divDelete.setAttribute('class', 'delete');
                }
                divDelete.onclick = function(){
                    _this.delButtonClicked.notify({id: id});
                };

                divBox.appendChild(inputCheckbox);
                labelCheckbox.appendChild(span);
                divBox.appendChild(labelCheckbox);
                divItem.appendChild(divBox);
                divItem.appendChild(h3);
                divItem.appendChild(divDelete);
                list.appendChild(divItem);

            }
        }
    },

    /**
     *  Clear text input field
     */
    inputTextClear: function () {
        this._elements.inputText.value = "";
    },

    /**
     *
     * @param {boolean} mode - Show/hide exclamation icon
     */
    setIconError: function(mode){
        var iconError = this._elements.iconError;
        if (mode){
            iconError.classList.remove('hidden')
        }
        else if (!iconError.classList.contains('hidden')){
            iconError.classList.add('hidden');
        }
    },

    /**
     *
     * @param {boolean} mode - Show/hide globe icon
     */
    setIconConnection: function(mode){
        var iconConnection = this._elements.iconConnection;
        if (mode){
            iconConnection.classList.remove('hidden')
        }
        else if (!iconConnection.classList.contains('hidden')){
            iconConnection.classList.add('hidden');
        }
    }

};