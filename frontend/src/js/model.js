import {Event} from './event'

/**
 * MVC applications model
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 * @constructor
 */
export function Model() {
    this._inputText = "";

    this.itemAdded = new Event(this);
    this.listChanged = new Event(this);
    this.inputTextUpdate = new Event(this);
    this.inputTextClear = new Event(this);
    this.connectionError = new Event(this);

    this.apiUrl = '/api'
}

Model.prototype = {

    /**
     *  Fetch from API list of all todos
     */
    fetchList: function () {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('GET', this.apiUrl + '/list', true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                _this._items = JSON.parse(request.responseText).list;
                _this.listChanged.notify();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send();
    },

    /**
     * Get list of items
     * @returns {object} - List with items
     */
    getItems: function () {
        return this._items;
    },

    /**
     * Send to API new item and refresh list.
     * @param {object} item - object with fields "tittle" and "done".
     */
    addItem: function (item) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('POST', this.apiUrl + '/todo', true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send(JSON.stringify({todo: {title: item.title, done: item.done}}));
    },

    /**
     * Request API to delete item with passed id and refresh list
     * @param {number} id - Id of deleting item
     */
    removeItem: function (id) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('DELETE', this.apiUrl + '/todo?id=' + id, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList()
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send();
    },

    /**
     * Request API to change "done" status of item with passed id and refresh list
     * @param {number} id - Id of updated item
     */
    doItem: function (id) {
        var _this = this;
        var item = this._items.find(function (item) {
            return item.id === id
        });
        var request = new XMLHttpRequest();
        request.open('PUT', this.apiUrl + '/todo', true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send(JSON.stringify({todo: {id: id, done: !item.done}}));
    },

    /**
     * Get input text
     * @returns {string}
     */
    getInputText: function () {
        return this._inputText;
    },

    /**
     * Set input text
     * @param text
     */
    setInputText: function (text) {
        this._inputText = text;
        this.inputTextUpdate.notify({text: text});
    },

    /**
     * Clear input text
     */
    clearInputText: function () {
        this._inputText = "";
        this.inputTextClear.notify();
    },

};


