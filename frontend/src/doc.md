<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

-   [Model](#model)
    -   [fetchList](#fetchlist)
    -   [getItems](#getitems)
    -   [addItem](#additem)
    -   [removeItem](#removeitem)
    -   [doItem](#doitem)
    -   [getInputText](#getinputtext)
    -   [setInputText](#setinputtext)
    -   [clearInputText](#clearinputtext)
-   [Event](#event)
    -   [attach](#attach)
    -   [notify](#notify)
-   [View](#view)
    -   [rebuildList](#rebuildlist)
    -   [inputTextClear](#inputtextclear)
    -   [setIconError](#seticonerror)
    -   [setIconConnection](#seticonconnection)
-   [Controller](#controller)
    -   [addItem](#additem-1)
    -   [delItem](#delitem)
    -   [doItem](#doitem-1)
    -   [updateInputText](#updateinputtext)

## Model

MVC applications model

Used MVC in VanillaJS template
from:
<https://alexatnet.com/model-view-controller-mvc-in-javascript/>

### fetchList

Fetch from API list of all todos

### getItems

Get list of items

Returns **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** List with items

### addItem

Send to API new item and refresh list.

**Parameters**

-   `item` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** object with fields "tittle" and "done".

### removeItem

Request API to delete item with passed id and refresh list

**Parameters**

-   `id` **[number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** Id of deleting item

### doItem

Request API to change "done" status of item with passed id and refresh list

**Parameters**

-   `id` **[number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** Id of updated item

### getInputText

Get input text

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** 

### setInputText

Set input text

**Parameters**

-   `text`  

### clearInputText

Clear input text

## Event

Implementation of observer pattern

**Parameters**

-   `sender` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** Object which emits events

### attach

Attach new listener

**Parameters**

-   `listener` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** callback function

### notify

Notify listeners about actions

**Parameters**

-   `args`  

## View

MVC applications view

Used MVC in VanillaJS template
from:
<https://alexatnet.com/model-view-controller-mvc-in-javascript/>

**Parameters**

-   `model` **[object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 
-   `elements` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** Object that contains handlers to DOM control elements

### rebuildList

Function which rebuilds list of todos
 It looks terrible, but it's made without external libs.
 Template rendering package would make code more pure.

### inputTextClear

Clear text input field

### setIconError

**Parameters**

-   `mode` **[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** Show/hide exclamation icon

### setIconConnection

**Parameters**

-   `mode` **[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)** Show/hide globe icon

## Controller

MVC aplications controller

Used MVC in VanillaJS template
from:
<https://alexatnet.com/model-view-controller-mvc-in-javascript/>

**Parameters**

-   `model` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 
-   `view` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 

### addItem

Add new item

### delItem

Delete item

**Parameters**

-   `id` **[number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)** Id of deleted item

### doItem

Change "done" state of item

**Parameters**

-   `id`  Id of updated item

### updateInputText

Update input state

**Parameters**

-   `text` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** Input text
