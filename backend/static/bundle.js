/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Event;
/**
 * Implementation of observer pattern
 * @param {Object} sender - Object which emits events
 * @constructor
 */
function Event(sender) {
    this._sender = sender;
    this._listeners = [];
}

Event.prototype = {

    /**
     * Attach new listener
     * @param {object} listener - callback function
     */
    attach : function (listener) {
        this._listeners.push(listener);
    },

    /**
     * Notify listeners about actions
     * @param args
     */
    notify : function (args) {
        var index;

        for (index = 0; index < this._listeners.length; index += 1) {
            this._listeners[index](this._sender, args);
        }
    }
};


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__view__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__controller__ = __webpack_require__(2);
//  Used for debugging and developing
// import '../../index.html'
// import '../sass/app.scss'







document.addEventListener('DOMContentLoaded', function() {
    var model = new __WEBPACK_IMPORTED_MODULE_0__model__["a" /* Model */](),
        view = new __WEBPACK_IMPORTED_MODULE_1__view__["a" /* View */](model, {
            'list' : document.getElementById('list'),
            'inputText': document.getElementById('input-text'),
            'addButton' : document.getElementById('add-new-todo'),
            'iconError' : document.getElementById('icon-error'),
            'iconConnection' : document.getElementById('icon-connection'),
        }),
        controller = new __WEBPACK_IMPORTED_MODULE_2__controller__["a" /* Controller */](model, view);
    model.fetchList();
    view.rebuildList();
});


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Controller;
/**
 * MVC aplications controller
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 *
 * @param {Object} model
 * @param {Object} view
 * @constructor
 */
function Controller(model, view) {
    var _this = this;

    this._model = model;
    this._view = view;

    this._view.addButtonClicked.attach(function () {
        _this.addItem();
    });

    this._view.delButtonClicked.attach(function (sender, args) {
        _this.delItem(args.id);
    });

    this._view.doneCheckboxChanged.attach(function (sender, args) {
        _this.doItem(args.id);
    });

    this._view.inputTextChanged.attach(function (sender, args) {
        _this.updateInputText(args.text);
    })
}

Controller.prototype = {

    /**
     * Add new item
     */
    addItem : function () {
        var item = {
            title: this._model.getInputText(),
            done: false
        };
        if (item.title.length > 1) {
            this._model.addItem(item);
            this._model.clearInputText();
            this._view.setIconError(false);
        }
        else{
            this._view.setIconError(true);
        }
    },

    /**
     * Delete item
     * @param {number} id - Id of deleted item
     */
    delItem : function (id) {
        this._model.removeItem(id);
    },
    /**
     * Change "done" state of item
     * @param id - Id of updated item
     */
    doItem : function (id) {
        this._model.doItem(id);
    },

    /**
     * Update input state
     * @param {string} text - Input text
     */
    updateInputText : function (text) {
        this._model.setInputText(text);
        if (text.length > 1)
            this._view.setIconError(false);
    }
};


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Model;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event__ = __webpack_require__(0);


/**
 * MVC applications model
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 * @constructor
 */
function Model() {
    this._inputText = "";

    this.itemAdded = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.listChanged = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.inputTextUpdate = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.inputTextClear = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.connectionError = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);

    this.apiUrl = '/api'
}

Model.prototype = {

    /**
     *  Fetch from API list of all todos
     */
    fetchList: function () {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('GET', this.apiUrl + '/list', true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                _this._items = JSON.parse(request.responseText).list;
                _this.listChanged.notify();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send();
    },

    /**
     * Get list of items
     * @returns {object} - List with items
     */
    getItems: function () {
        return this._items;
    },

    /**
     * Send to API new item and refresh list.
     * @param {object} item - object with fields "tittle" and "done".
     */
    addItem: function (item) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('POST', this.apiUrl + '/todo', true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send(JSON.stringify({todo: {title: item.title, done: item.done}}));
    },

    /**
     * Request API to delete item with passed id and refresh list
     * @param {number} id - Id of deleting item
     */
    removeItem: function (id) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.open('DELETE', this.apiUrl + '/todo?id=' + id, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList()
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send();
    },

    /**
     * Request API to change "done" status of item with passed id and refresh list
     * @param {number} id - Id of updated item
     */
    doItem: function (id) {
        var _this = this;
        var item = this._items.find(function (item) {
            return item.id === id
        });
        var request = new XMLHttpRequest();
        request.open('PUT', this.apiUrl + '/todo', true);
        request.setRequestHeader("Content-Type", "application/json");
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && JSON.parse(request.responseText).success) {
                _this.fetchList();
                _this.connectionError.notify({status: false});
            } else {
                _this.connectionError.notify({status: true});
            }
        };
        request.onerror = function () {
            _this.connectionError.notify({status: true});
        };
        request.send(JSON.stringify({todo: {id: id, done: !item.done}}));
    },

    /**
     * Get input text
     * @returns {string}
     */
    getInputText: function () {
        return this._inputText;
    },

    /**
     * Set input text
     * @param text
     */
    setInputText: function (text) {
        this._inputText = text;
        this.inputTextUpdate.notify({text: text});
    },

    /**
     * Clear input text
     */
    clearInputText: function () {
        this._inputText = "";
        this.inputTextClear.notify();
    },

};




/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = View;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event__ = __webpack_require__(0);


/**
 * MVC applications view
 *
 * Used MVC in VanillaJS template
 * from:
 * https://alexatnet.com/model-view-controller-mvc-in-javascript/
 *
 * @param {object} model
 * @param {Object} elements - Object that contains handlers to DOM control elements
 * @constructor
 */

function View(model, elements) {
    var _this = this;

    this._model = model;
    this._elements = elements;

    this.addButtonClicked = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.delButtonClicked = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.doneCheckboxChanged = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);
    this.inputTextChanged = new __WEBPACK_IMPORTED_MODULE_0__event__["a" /* Event */](this);


    this._model.itemAdded.attach(function () {
        _this.rebuildList();
    });
    this._model.listChanged.attach(function () {
        _this.rebuildList();
    });
    this._model.inputTextClear.attach(function () {
        _this.inputTextClear();
    })
    this._model.connectionError.attach(function (sender, args) {
        _this.setIconConnection(args.status)
    })

    this._elements.addButton.onclick = function () {
        _this.addButtonClicked.notify();
    };

    this._elements.inputText.onkeyup = function (e) {
        _this.inputTextChanged.notify({text: e.target.value});
    }
}

View.prototype = {

    /**
     *  Function which rebuilds list of todos
     *  It looks terrible, but it's made without external libs.
     *  Template rendering package would make code more pure.
     */
    rebuildList: function () {
        var list, items, key;

        list = this._elements.list;
        list.innerHTML = '';

        items = this._model.getItems();
        for (key in items) {
            if (items.hasOwnProperty(key)) {
                let id = items[key].id;
                var _this = this;

                var divItem = document.createElement('div');
                divItem.setAttribute('class', 'item');
                var divBox = document.createElement('div');
                divBox.setAttribute('class', 'box');
                var inputCheckbox = document.createElement('input');
                inputCheckbox.setAttribute('type', 'checkbox');
                inputCheckbox.setAttribute('id', 'item-select-' + items[key].id);
                if (items[key].done){
                    inputCheckbox.setAttribute('checked', true);
                }
                inputCheckbox.onchange = function(){
                    _this.doneCheckboxChanged.notify({id: id});
                };
                var labelCheckbox = document.createElement('label');
                labelCheckbox.setAttribute('for', 'item-select-' + items[key].id);
                var span = document.createElement('span');
                span.setAttribute('class', 'checkbox');
                var h3 = document.createElement('h3');
                if (items[key].done){
                    h3.setAttribute('class', 'done')
                }
                var text = document.createTextNode(items[key].title);
                h3.appendChild(text);
                var divDelete = document.createElement('div');
                if (items[key].done) {
                    divDelete.setAttribute('class', 'delete done');
                }
                else{
                    divDelete.setAttribute('class', 'delete');
                }
                divDelete.onclick = function(){
                    _this.delButtonClicked.notify({id: id});
                };

                divBox.appendChild(inputCheckbox);
                labelCheckbox.appendChild(span);
                divBox.appendChild(labelCheckbox);
                divItem.appendChild(divBox);
                divItem.appendChild(h3);
                divItem.appendChild(divDelete);
                list.appendChild(divItem);

            }
        }
    },

    /**
     *  Clear text input field
     */
    inputTextClear: function () {
        this._elements.inputText.value = "";
    },

    /**
     *
     * @param {boolean} mode - Show/hide exclamation icon
     */
    setIconError: function(mode){
        var iconError = this._elements.iconError;
        if (mode){
            iconError.classList.remove('hidden')
        }
        else if (!iconError.classList.contains('hidden')){
            iconError.classList.add('hidden');
        }
    },

    /**
     *
     * @param {boolean} mode - Show/hide globe icon
     */
    setIconConnection: function(mode){
        var iconConnection = this._elements.iconConnection;
        if (mode){
            iconConnection.classList.remove('hidden')
        }
        else if (!iconConnection.classList.contains('hidden')){
            iconConnection.classList.add('hidden');
        }
    }

};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);