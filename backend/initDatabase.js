var sqlite3 = require('sqlite3').verbose()
var db = new sqlite3.Database('db.sqlite3')

db.serialize(function () {
    db.run("CREATE TABLE IF NOT EXISTS todo ( id INTEGER PRIMARY KEY   AUTOINCREMENT, title TEXT, done INTEGER)");
    var stmt = db.prepare('INSERT INTO todo (title, done) VALUES (?, ?)');
    var rows = [
        ['Lorem ipsum dolor sit amet,', 0],
        ['Cras at ipsum maximus, tincidunt turpis sed,', 1],
        ['Suspendisse augue neque, venenatis ', 0],
        ['Duis vitae odio tristique, rhoncus', 1],
    ]
    rows.forEach((row) => {
        stmt.run(row);
    });
    stmt.finalize()
    db.each('SELECT * FROM todo', function (err, row) {
        console.log(row)
    })
});
db.close();