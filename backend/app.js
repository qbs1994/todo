var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose()
var db = new sqlite3.Database('db.sqlite3')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 80;
var router = express.Router();

/**
 * Name: Get api info
 * Route: '/'
 * Method: Get
 * Response: Json with api information
 *
 */
router.get('/', function (req, res) {
    res.json({
        api: [{
                name: 'Todo List',
                type: 'GET',
                url: '/list'
            },
            {
                name: 'Add Todo',
                type: 'POST',
                url:  '/todo'
            },
            {
                name: 'Delete Todo',
                type: 'DELETE',
                url:  '/todo',
            },
            {
                name: 'Update Todo',
                type: 'PUT',
                url:  '/todo',
            },
            {
                name: 'Get Todo',
                type: 'GET',
                url:  '/todo'
            },
        ]
    });
});

/**
 * Name: Get todo list
 * Route: '/list'
 * Method: Get
 * Response: {success:boolean, list:[{title:string, done:boolean, id:number}]} - Json with list of items
 */
router.get('/list', function (req, res) {
    db.all('SELECT * FROM todo', function (err, list) {
        res.json({success: true, list});
    })
});

/**
 * Name: Get todo
 * Route: '/todo'
 * Method: Get
 * Query args: id = number
 * Response: {success:boolean, todo:{title:string, done:boolean, id:number}} - Json with todo
 */
router.get('/todo', function (req, res) {
    if (!req.query.id)
        res.json({success: false, msg: "No id provided"});
    else if ( isNaN(req.query.id))
        res.json({success: false, msg: "id is not a number"});
    else{
        db.get('SELECT * FROM todo WHERE id = ?',req.query.id ,function (err, todo) {
            todo ? res.json({success: true, todo})
                : res.json({success: false, msg: 'No todo with id ' + req.query.id});
        });
    }
});

/**
 * Name: Add todo
 * Route: '/todo'
 * Method: Post
 * Body:  {todo:{title:string, done:boolean}}
 * Response: {success:boolean, msg:string}
 */
router.post('/todo', function (req, res) {
    if (!req.body.todo)
        res.json({success: false, msg: "No todo provided"});
    else if ( typeof(req.body.todo.title) === 'undefined' || typeof(req.body.todo.done) === 'undefined')
        res.json({success: false, msg: "No title or done field in provided todo"});
    else if ( req.body.todo.title.length < 2)
        res.json({success: false, msg: "Title length to short"});
    else {
        db.run('INSERT INTO todo (title, done) VALUES (?, ?)', [req.body.todo.title, req.body.todo.done], function(err){
            if(err)
                res.json({success: false, msg: err.code});
            else
                res.json({success: true});
        });
    }
});

/**
 * Name: Delete todo
 * Route: '/todo'
 * Method: Delete
 * Query args: id = number
 * Response: {success:boolean, msg:string}
 */
router.delete('/todo', function (req, res) {
    if (!req.query.id)
        res.json({success: false, msg: "No id provided"});
    else if ( isNaN(req.query.id))
        res.json({success: false, msg: "id is not a number"});
    else{
        db.run('DELETE FROM todo WHERE id = ?',req.query.id ,function (err) {
            if(err)
                res.json({success: false, msg: err.code});
            else
                res.json({success: true});
        });
    }
});

/**
 * Name: Update todo
 * Route: '/todo'
 * Method: Put
 * Body:  {todo:{id:number, done:boolean}}
 * Response: {success:boolean, msg:string}
 */
router.put('/todo', function (req, res) {
    if (!req.body.todo)
        res.json({success: false, msg: "No todo provided"});
    else if ( typeof(req.body.todo.id) === 'undefined' || typeof(req.body.todo.done) === 'undefined')
        res.json({success: false, msg: "No id or done field in provided todo"});
    else {
        db.run('UPDATE todo SET done = ? WHERE id = ?', [req.body.todo.done ? 1 : 0, req.body.todo.id], function(err){
            if(err)
                res.json({success: false, msg: err.code});
            else
                res.json({success: true});
        });
    }
});


app.use('/api', router);
app.use(express.static('static'))

app.listen(port);
console.log('App working on port: ' + port);


